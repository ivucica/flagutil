// Package flagutil parses flagfiles and env into flagsets of the standard Go
// 'flag' package.
//
// It attempts to minimize conflict with the use of the standard package. In
// typical use case (read flagfile first, read environment next, and finally
// do typical processing of the command line flags), it is sufficient to replace
// the call to flag.Parse() with flagutil.Parse().
package flagutil

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

var (
	EnvironmentPrefix = ""
	FlagFileFlagName  = "flag_file"
	FlagFile          = flag.String(FlagFileFlagName, "", "flag file containing options as if specified on the command line, except new-line-delimited")
)

func Parse() {
	ParseDefaultFlagFile(flag.CommandLine)
	ParseDefaultEnv(flag.CommandLine)
	flag.Parse()
}

// ParseDefaultFlagFile opens the file file passed in the --flag_file command
// line flag, and processes it using ParseFlagFile.
//
// This is expected to be called before parsing the passed flag set, which may
// be especially problematic if this is the flag.CommandLine flagset. Therefore,
// special care is taken to process just --flag_file separately.
//
// Additionally, environment is checked for the flag named EnvironmentPrefix
// + "_FLAG_FILE" (with prefix uppercased), or, if prefix is unset, just
// "FLAG_FILE".
//
// Name of flag "--flag_file" can be overridden in the global variable
// FlagFileFlagName. This is taken into account when looking into the
// environment, and is adequately uppercased.
//
// Flag file description can be seen in docstring for ParseFlagsFromReader.
//
// Currently, only the last "--flag_file" will be taken into account. In future,
// multiple values may be processed, and in addition to the environment's
// FLAG_FILE.
func ParseDefaultFlagFile(fs *flag.FlagSet) error {
	flagFilePath := ""

	if EnvironmentPrefix == "" {
		if value, ok := os.LookupEnv(strings.ToUpper(FlagFileFlagName)); ok {
			flagFilePath = value
		}
	} else {
		if value, ok := os.LookupEnv(strings.ToUpper(EnvironmentPrefix) + "_" + strings.ToUpper(FlagFileFlagName)); ok {
			flagFilePath = value
		}
	}

	flagFileNext := false
	for _, arg := range os.Args {
		if flagFileNext {
			flagFilePath = arg
			flagFileNext = false
			continue
		}

		if strings.HasPrefix(arg, "--"+FlagFileFlagName+"=") {
			flagFilePath = arg[len("--"+FlagFileFlagName+"="):]
		} else if strings.HasPrefix(arg, "-"+FlagFileFlagName+"=") {
			flagFilePath = arg[len("-"+FlagFileFlagName+"="):]
		} else if arg == "--"+FlagFileFlagName || arg == "-"+FlagFileFlagName {
			flagFileNext = true
		}
	}

	if flagFilePath != "" {
		return ParseFlagFile(fs, flagFilePath)
	}
	return nil
}

// ParseFlagFile opens the file from specified path and parses the flags in the
// file into the passed flagset.
//
// Flag file description can be seen in docstring for ParseFlagsFromReader.
func ParseFlagFile(fs *flag.FlagSet, flagFilePath string) error {
	flagFile, err := os.Open(flagFilePath)
	if err != nil {
		log.Printf("warning: failed to open passed flag file %s: %s\n", flagFilePath, err)
		return fmt.Errorf("parse flag file: %v: %v", flagFilePath, err)
	}
	defer flagFile.Close()
	return ParseFlagsFromReader(fs, flagFile)
}

// ParseFlagsFromReader accessed the already-opened reader and reads it in its
// entirety. Each line is considered to be a new flag. Empty lines and lines
// beginning with '#' are ignored. Each line is trimmed, so lines consisting of
// just whitespace, or starting with whitespace and '#', will also be skipped.
//
// At this time, there is no way to pass a flag value ending with whitespace.
// This may change.
func ParseFlagsFromReader(fs *flag.FlagSet, reader io.Reader) error {
	r := bufio.NewReader(reader)

	var (
		flags []string
		line  string
		err   error
	)
	for line, err = r.ReadString('\n'); err == nil; line, err = r.ReadString('\n') {
		line = strings.TrimSpace(line)
		if line == "" || line[0] == '#' {
			continue
		}
		flags = append(flags, line)
	}
	if err != nil && err != io.EOF {
		return fmt.Errorf("flag.ParseFlagsFromReader: reading: %v", err)
	}
	if err == io.EOF {
		// Some content may have been read. This may happen if the file
		// does not end in a newline.
		line = strings.TrimSpace(line)
		if !(line == "" || line[0] == '#') {
			flags = append(flags, line)
		}
	}

	err = fs.Parse(flags)
	if err != nil {
		return fmt.Errorf("flag.ParseFlagsFromReader: Parse: %v", err)
	}
	return nil
}

// ParseDefaultEnv iterates through every flag in the passed flagset, makes the
// flag names uppercase, and checks if the passed flag is in the default
// environment using os.LookupEnv. If so, it is set in the flagset.
//
// If the global variable of this package called EnvironmentPrefix is set, then
// it is uppercased and prepended along with an underscore.
//
// For instance, if a flag is called 'user_name' and the EnvironmentPrefix is
// set to 'simple', then the environment variable checked will be called
// 'SIMPLE_USER_NAME'.
func ParseDefaultEnv(fs *flag.FlagSet) error {
	fs.VisitAll(func(f *flag.Flag) {
		name := strings.ToUpper(f.Name)
		if EnvironmentPrefix != "" {
			name = strings.ToUpper(EnvironmentPrefix) + "_" + name
		}
		if val, ok := os.LookupEnv(name); ok {
			fs.Set(f.Name, val)
		}
	})
	return nil
}
