# flagutil

Package flagutil parses flagfiles and env into flagsets of the standard Go
'flag' package.

It attempts to minimize conflict with the use of the standard package. In
typical use case (read flagfile first, read environment next, and finally
do typical processing of the command line flags), it is sufficient to replace
the call to flag.Parse() with flagutil.Parse().

Author: Ivan Vucica

[Documentation](http://godoc.org/badc0de.net/pkg/flagutil/v1)

## License

This package has no license at this time.
