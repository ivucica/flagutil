#!/bin/bash
set -e
go get badc0de.net/pkg/flagutil/examples/flagutil_simple

echo "Default run:"
${GOPATH}/bin/flagutil_simple
echo "Flag on command line:"
${GOPATH}/bin/flagutil_simple --name="Jerry"
echo "Flag from environment:"
NAME=Jack ${GOPATH}/bin/flagutil_simple
echo "Flag from flagfile (temporarily created by bash):"
${GOPATH}/bin/flagutil_simple --flag_file=<(echo "--name=Joe")
