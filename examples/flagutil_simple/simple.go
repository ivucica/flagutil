package main

import (
	"badc0de.net/pkg/flagutil"
	"flag"
	"fmt"

	"os/user"
)

var (
	name = flag.String("name", defaultUserName(), "user's name")
)

func defaultUserName() string {
	user, err := user.Current()
	if err != nil {
		return "nobody"
	}
	if user.Name != "" {
		return user.Name
	}
	return user.Username
}

func main() {
	flagutil.Parse()

	fmt.Printf("Hello, %s!\n", *name)
}
