#!/bin/bash
set -e
go get badc0de.net/pkg/flagutil/examples/flagutil_multiple

${GOPATH}/bin/flagutil_multiple --name=Jerry
${GOPATH}/bin/flagutil_multiple --name=Newman --greeting=...hello
GREETING=Hi ${GOPATH}/bin/flagutil_multiple --name=world
${GOPATH}/bin/flagutil_multiple --flag_file=<(printf -- '--name=Joe\n--greeting=Ohai\n')
NAME=ThisWontHappen GREETING=Bazinga ${GOPATH}/bin/flagutil_multiple --flag_file=<(printf -- '--name=ThisWillBeIgnored\n--greeting=ThisWillBeOverridden\n') --name=Jen

