# flagutil

Package flagutil parses flagfiles and env into flagsets of the standard Go
'flag' package.

It attempts to minimize conflict with the use of the standard package. In
typical use case (read flagfile first, read environment next, and finally
do typical processing of the command line flags), it is sufficient to replace
the call to flag.Parse() with flagutil.Parse().

Author: Ivan Vucica

[Documentation](http://godoc.org/badc0de.net/pkg/flagutil)

## DEPRECATION

Note that `badc0de.net/pkg/flagutil/v1` (with the `v1` suffix) is deprecated
and will be removed at some point.

Currently, this package contains a near-copy of the canonical version, which
prints a warning upon call to `flagutil.Parse()`.

This facilitates the move to go modules soon, per the "Semantic import
versioning" section of

  http://web.archive.org/web/20200303201624/https://github.com/golang/go/wiki/Modules

which states

  If the module is version v0 or v1, do _not_ include the major version
  in either the module path or the import path.

## License

This package has no license at this time.
